= Quick Start: building and installing GHC =

If you are an aspiring GHC developer, you may want to read the [wiki:Contributing Contributing] page first.

To build GHC, first make sure to:
  [wiki:Building/Preparation Prepare your machine]
For Windows users, all the commands below must be executed in the MinGW shell, not Command Prompt or PowerShell.

Then, get the sources by cloning GHC's main Git repository or via an alternative method (see [wiki:Building/GettingTheSources Getting the Sources] for more details). Note: you can connect to GitLab using SSH, if you register to GitLab and upload your SSH key. In that case the following line would have "git:" instead of "https:"

{{{
#!sh
git clone --recursive https://gitlab.haskell.org/ghc/ghc
cd ghc/
}}}

Unless you want to build the latest development version of GHC, [#CheckoutStable checkout a stable release].

Optionally, you can customize your build by creating the file `mk/build.mk` using `mk/build.mk.sample` as a template and e.g. removing the comment marker # on the line "BuildFlavour = devel2".

If you obtained the repository via `git clone --recursive`, you will need to first run:

{{{#!sh
$ ./boot
}}}

,,This step isn't necessary if you obtained the source from a tar archive.,,

Next, run the `./configure` script followed by `make` to start the build:

{{{#!sh
$ ./configure # Windows users must append "--enable-tarballs-autodownload"
$ make -j8 # Use 8 parallel jobs; adapt to actual number of cpu cores
}}}

,,On Windows you need to download some binary distributables before being able to build.  This only has to be done once and can be done by adding the `--enable-tarballs-autodownload` flag to the call to `./configure`.,,

By default (without any `mk/build.mk` customization), this will do a 2-stage bootstrap build of the compiler, with profiling libraries.

== Run without installing ==

You can find the binaries built by make in the `inplace/bin` directory under the root of the ghc source tree. The binaries can be run from here without installing.

== Installing ==

After building, to install GHC (by default in `/usr/local`):

{{{#!sh
$ make install
}}}

You may need to use `sudo`.


== Fast rebuilding ==
There are 4 things to remember:
 1. Select `BuildFlavour = devel2` in your `mk/build.mk` file (copy `mk/build.mk.sample` to `mk/build.mk` first), to
    [Building/Using#HowtomakeGHCbuildquickly make GHC build more quickly].

 2. Don't run `make` directly in the ghc root directory (unless you just pulled in changes from others). Instead, first
    change to the directory (`compiler`, `utils`, `ghc` or `libraries`) where you're making your changes.
    See [Building/Using#Buildingasinglesub-component Building a single sub-component].
 
 3. Set `stage=2` in your `mk/build.mk` file, to
    [wiki:Building/Using#Freezingstage1 freeze the stage 1 compiler].
    This makes sure that only the
    [wiki:Building/Architecture/Idiom/Stages stage-2] compiler will be
    rebuilt after this.

 4. While in the sub-component directory, use `make fast`
    [wiki:Building/Using#Skipdependencybuilding skip dependency building] (except after pulling in changes from others).

A good first sanity check is to twiddle some error message in the code, just to see that changed error message pop up when you compile a file. Write some Haskell code with an error in it, and look at the error message. Search through the code for that error message. Change the message, rebuild ghc (run `make fast` in the `ghc` directory), and recompile your file again with `./inplace/bin/ghc-stage2`. If you see the changed message, you're good to go.


[=#CheckoutStable]
== Check out a Stable Release ==

You can learn what branches are available via ```git branch -a```. You can e.g. switch to the latest stable release of GHC-8.6.x:

{{{
#!sh
git checkout ghc-8.6
}}}

You'll then have to update the git submodules:

{{{
#!sh
git submodule update --init
}}}

== More information ==

Has your question not been answered? See [[Building]] for more resources. Also, don't hestitate to [[MailingListsAndIRC|ask for help]].